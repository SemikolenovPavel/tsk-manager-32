package ru.t1.semikolenov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.t1.semikolenov.tm.dto.request.TaskBindToProjectRequest;
import ru.t1.semikolenov.tm.dto.request.TaskUnbindFromProjectRequest;
import ru.t1.semikolenov.tm.dto.response.TaskBindToProjectResponse;
import ru.t1.semikolenov.tm.dto.response.TaskUnbindFromProjectResponse;

public interface IProjectTaskEndpoint {

    @NotNull
    TaskBindToProjectResponse bindTaskToProject(@NotNull TaskBindToProjectRequest request);

    @NotNull
    TaskUnbindFromProjectResponse unbindTaskFromProject(@NotNull TaskUnbindFromProjectRequest request);

}