package ru.t1.semikolenov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.Task;

public final class TaskUpdateByIdResponse extends AbstractTaskResponse {

    public TaskUpdateByIdResponse(@Nullable final Task task) {
        super(task);
    }

}