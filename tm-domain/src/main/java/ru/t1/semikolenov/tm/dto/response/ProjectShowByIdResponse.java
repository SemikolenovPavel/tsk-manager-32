package ru.t1.semikolenov.tm.dto.response;

import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.Project;

public final class ProjectShowByIdResponse extends AbstractProjectResponse {

    public ProjectShowByIdResponse(@Nullable final Project project) {
        super(project);
    }

}