package ru.t1.semikolenov.tm.exception;

public abstract class AbstractException extends RuntimeException {

    public AbstractException(String message) {
        super(message);
    }

}