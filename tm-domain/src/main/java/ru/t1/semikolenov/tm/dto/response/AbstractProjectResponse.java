package ru.t1.semikolenov.tm.dto.response;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import ru.t1.semikolenov.tm.model.Project;

@Getter
public abstract class AbstractProjectResponse extends AbstractResponse {

    @Nullable
    private Project project;

    public AbstractProjectResponse(@Nullable final Project project) {
        this.project = project;
    }

}